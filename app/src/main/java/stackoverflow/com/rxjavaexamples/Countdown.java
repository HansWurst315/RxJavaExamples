package stackoverflow.com.rxjavaexamples;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;
import stackoverflow.com.rxjavaexamples.events.CountdownEvent;
import stackoverflow.com.rxjavaexamples.events.EndCountdownEvent;
import stackoverflow.com.rxjavaexamples.events.ExtendCountdownEvent;
import stackoverflow.com.rxjavaexamples.events.StartCountdownEvent;
import stackoverflow.com.rxjavaexamples.events.ValueCountdownEvent;

// TODO: Capture time when last value has been published.
// TODO: Calculate 1 - value.getMilli()
// TODO: Use diff value as delay for publishing first value of extend-stream
class Countdown {
    private final ConnectableObservable<CountdownEvent> eventProducer;
    private final Observable<Boolean> isRunning;
    private final Observable<Integer> countDownObservable;

    private Countdown(Observable<CountdownEvent> eventProducer) {
        Observable<CountdownEvent> sharedEventProducer = eventProducer.share();

        this.eventProducer = sharedEventProducer.startWith(new EndCountdownEvent())
                .replay(1);

        // HACK: because exp. final
        final Recursive<ConnectableObservable<Integer>> rec = new Recursive<>();

        Observable<CountdownEvent> startCountdownValues = this.eventProducer.switchMap(event -> {
            if (event instanceof StartCountdownEvent) {
                StartCountdownEvent startEvent = (StartCountdownEvent) event;
                return createTimer(startEvent.getStartValue())
                        .<CountdownEvent>map(ValueCountdownEvent::new)
                        .concatWith(Observable.timer(1000, TimeUnit.MILLISECONDS).map(aLong -> new EndCountdownEvent()));
            } else if (event instanceof ExtendCountdownEvent) {
                ExtendCountdownEvent extendValue = (ExtendCountdownEvent) event;

                return rec.func.take(1)
                        .flatMap(integer -> createTimer(integer + extendValue.getBy())
                                .<CountdownEvent>map(ValueCountdownEvent::new)
                                .concatWith(Observable.timer(1000, TimeUnit.MILLISECONDS).map(aLong -> new EndCountdownEvent())));
            }

            Log.v("Countdown", "startCountdownValues return");
            return Observable.just(new EndCountdownEvent());
        });

        // Caches last value from ValueCountdownEvent stream
        rec.func = startCountdownValues
                .ofType(ValueCountdownEvent.class)
                .map(ValueCountdownEvent::getValue)
                .replay(1);

        ConnectableObservable<Boolean> isRunningConnectable = Observable.merge(startCountdownValues, this.eventProducer)
                .filter(countdownEvent -> countdownEvent instanceof StartCountdownEvent || countdownEvent instanceof EndCountdownEvent)
                .map(countdownEvent -> {
                    if (countdownEvent instanceof StartCountdownEvent) {
                        return true;
                    } else if (countdownEvent instanceof EndCountdownEvent) {
                        return false;
                    }
                    throw new IllegalArgumentException("fuck you");
                })
                .doOnSubscribe(disposable -> {
                    Log.v("Countdown", "subscribed");
                })
                .doOnNext(aBoolean -> {
                    Log.v("Countdown", "isRunningConnectable --- " + aBoolean);
                })
                .distinctUntilChanged()
                .replay(1); // NOTE: caches last value

        rec.func.connect();
        this.eventProducer.connect();
        isRunningConnectable.connect();

        this.isRunning = isRunningConnectable;
        this.countDownObservable = rec.func;
    }

    static Countdown create(Observable<CountdownEvent> eventProducer) {
        return new Countdown(eventProducer);
    }

    /**
     * @return hot observable
     */
    Observable<CountdownEvent> observeEvents() {
        return this.eventProducer;
    }

    private Observable<Integer> createTimer(int start) {
        return Observable.interval(0, 1, TimeUnit.SECONDS)
                .map(aLong -> (int) (start - aLong))
                .take(1 + start)
                .zipWith(Observable.range(0, start), (aLong, integer) -> {
                    return aLong;
                });
    }

    /**
     * @return hot observable
     */
    Observable<Boolean> isRunning() {
        return this.isRunning;
    }

    /**
     * @return hot observable
     */
    Observable<Integer> observeCounter() {
        return this.countDownObservable;
    }
}