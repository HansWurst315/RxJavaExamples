package stackoverflow.com.rxjavaexamples.events;

public class ExtendValueCountdownEvent implements CountdownEvent {
    private final ExtendCountdownEvent extend;
    private final ValueCountdownEvent value;

    public ExtendValueCountdownEvent(ExtendCountdownEvent extend, ValueCountdownEvent value) {
        this.extend = extend;
        this.value = value;
    }

    @Override
    public String getName() {
        return null;
    }

    public ExtendCountdownEvent getExtend() {
        return extend;
    }

    public ValueCountdownEvent getValue() {
        return value;
    }
}
