package stackoverflow.com.rxjavaexamples.events;

import stackoverflow.com.rxjavaexamples.Marker;

@Marker
public interface CountdownEvent {
    String getName();
}