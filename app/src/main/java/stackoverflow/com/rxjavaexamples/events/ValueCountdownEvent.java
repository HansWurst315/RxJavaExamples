package stackoverflow.com.rxjavaexamples.events;

public final class ValueCountdownEvent implements CountdownEvent {
    private static String EVENT_NAME = "ValueCountdownEvent";

    private final int value;

    public ValueCountdownEvent(int value) {
        this.value = value;
    }

    @Override
    public String getName() {
        return EVENT_NAME;
    }

    public int getValue() {
        return value;
    }
}
