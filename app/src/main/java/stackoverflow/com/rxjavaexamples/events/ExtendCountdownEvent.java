package stackoverflow.com.rxjavaexamples.events;

public final class ExtendCountdownEvent implements CountdownEvent {
    private static String EVENT_NAME = "ExtendCountdownEvent";

    private final int by;

    public ExtendCountdownEvent(int by) {
        this.by = by;
    }

    public int getBy() {
        return by;
    }

    @Override
    public String getName() {
        return EVENT_NAME;
    }
}