package stackoverflow.com.rxjavaexamples.events;

public final class StartCountdownEvent implements CountdownEvent {
    private static String EVENT_NAME = "StartCountdownEvent";

    private final int startValue;
    private final int count;

    public StartCountdownEvent(int startValue, int count) {
        this.startValue = startValue;
        this.count = count;
    }

    public int getStartValue() {
        return startValue;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String getName() {
        return EVENT_NAME;
    }
}