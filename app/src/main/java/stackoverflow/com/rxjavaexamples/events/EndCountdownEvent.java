package stackoverflow.com.rxjavaexamples.events;


public final class EndCountdownEvent implements CountdownEvent {
    private static String EVENT_NAME = "EndCountdownEvent";

    @Override
    public String getName() {
        return EVENT_NAME;
    }
}