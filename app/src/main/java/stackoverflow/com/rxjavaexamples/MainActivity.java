package stackoverflow.com.rxjavaexamples;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxSeekBar;

import java.util.concurrent.TimeUnit;

import hu.akarnokd.rxjava2.operators.FlowableTransformers;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import stackoverflow.com.rxjavaexamples.events.CountdownEvent;
import stackoverflow.com.rxjavaexamples.events.ExtendCountdownEvent;
import stackoverflow.com.rxjavaexamples.events.StartCountdownEvent;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());

        TextView counterValueText = findViewById(R.id.counterValue);
        Button startButton = findViewById(R.id.startButton);
        Button extendButton = findViewById(R.id.extendButton);

        SeekBar seekBar = findViewById(R.id.extendValueSeekBar);
        TextView extendValueText = findViewById(R.id.extendValueText);

        Observable<Integer> seekBarChanges = RxSeekBar.userChanges(seekBar)
                .throttleLast(100, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .replay(1)
                .autoConnect();

        seekBarChanges
                .subscribe(seekBarChangeEvent -> {
                    extendValueText.setText(String.valueOf(seekBarChangeEvent));
                });

        // Setup StartButton-ValueProducer
        Observable<StartCountdownEvent> startEventProducer = RxView.clicks(startButton)
                .map(o -> new StartCountdownEvent(10, 5))
                .doOnNext(startCountdownEvent -> {
                    Log.v("Countdown:startButton", "startButton pressed: " + startCountdownEvent.getStartValue());
                });

        // Setup EndButton-ValueProducer
        Observable<ExtendCountdownEvent> extendEventProducer = RxView.clicks(extendButton).switchMap(o -> {
            return seekBarChanges.take(1).map(ExtendCountdownEvent::new);
        }).doOnNext(event -> Log.v("Countdown:extendBut", "extend pressed: " + event.getBy()));

        // Merge start/ extend-ValueProducer to pass in to Countdown
        Observable<CountdownEvent> eventProducer = Observable.merge(startEventProducer, extendEventProducer);

        // Init countdown
        Countdown countdown = Countdown.create(eventProducer);

        // Subscribe to countdown events to print to log
        countdown.observeEvents().subscribe(countdownEvent -> {
            Log.v("Countdown:events", String.format("EventType: %1$s", countdownEvent.getName()));
        });

        // Connect one subscriber
        countdown.isRunning()
                .subscribe();

        // Connect second subscriber -- If counter is not running, textView will not be visible
        // Set UI-Buttons
        countdown.isRunning()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    Log.v("Countdown:isRunning", String.format("IsCounterRunning: %1$s", aBoolean));

                    if (aBoolean) {
                        Log.v("Countdown:isRunning", "set visibility");

                        counterValueText.setVisibility(TextView.VISIBLE);
                    } else {
                        counterValueText.setVisibility(TextView.INVISIBLE);
                        counterValueText.setText(String.valueOf(""));
                    }

                    extendButton.setEnabled(aBoolean);
                    startButton.setEnabled(!aBoolean);
                });

        // If countdown is Running -> set value of textView
        // FlowableTransformers.valve: only let through values if isRunning is true
        countdown.observeCounter()
                .toFlowable(BackpressureStrategy.DROP)
                .compose(FlowableTransformers.valve(countdown.isRunning().toFlowable(BackpressureStrategy.DROP)))
                .distinctUntilChanged()
                .doOnNext(integer -> {
                    Log.v("Countdown:value", "produce value " + integer);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integer -> {
                    counterValueText.setText(String.valueOf(integer));
                }, Throwable::printStackTrace);
    }
}
