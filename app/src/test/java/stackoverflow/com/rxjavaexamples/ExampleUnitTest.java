package stackoverflow.com.rxjavaexamples;

import org.junit.Test;


import io.reactivex.Observable;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void name() throws Exception {

    }

    interface Countdown {
        Observable<Integer> getCountdown();
    }

    public class CountDownImpl implements Countdown {

        public CountDownImpl() {
        }

        @Override
        public Observable<Integer> getCountdown() {
            return null;
        }
    }
}