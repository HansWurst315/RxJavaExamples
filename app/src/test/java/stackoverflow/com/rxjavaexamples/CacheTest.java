package stackoverflow.com.rxjavaexamples;

import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.BehaviorSubject;

public class CacheTest {
    @Test
    public void name() throws Exception {
        BehaviorSubject<Boolean> aDefault = BehaviorSubject.createDefault(false);

        ConnectableObservable<Boolean> isRunning = aDefault.doOnSubscribe(disposable -> System.out.println("sub"))
                .replay(1);

        TestObserver<Boolean> test1 = isRunning.test();
        TestObserver<Boolean> test2 = isRunning.test();

        isRunning.connect();

        aDefault.onNext(true);

        test1.assertValues(false, true);
        test2.assertValues(false, true);

        TestObserver<Boolean> test3 = isRunning.test().assertValue(true);
    }

    @Test
    public void name2() throws Exception {
        BehaviorSubject<Boolean> aDefault = BehaviorSubject.createDefault(false);

        Observable<Boolean> isRunning = aDefault.doOnSubscribe(disposable -> System.out.println("sub"));

        TestObserver<Boolean> test1 = isRunning.test();
        TestObserver<Boolean> test2 = isRunning.test();

        test1.assertValues(false).assertNotComplete();
        test2.assertValues(false).assertNotComplete();

        aDefault.onNext(true);

        test1.assertValues(false, true).assertNotComplete();
        test2.assertValues(false, true).assertNotComplete();

        TestObserver<Boolean> test3 = isRunning.test().assertValue(true);
    }
}
