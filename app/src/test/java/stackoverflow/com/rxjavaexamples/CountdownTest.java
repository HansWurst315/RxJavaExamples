package stackoverflow.com.rxjavaexamples;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Timed;
import io.reactivex.subjects.PublishSubject;
import stackoverflow.com.rxjavaexamples.events.CountdownEvent;
import stackoverflow.com.rxjavaexamples.events.StartCountdownEvent;

public class CountdownTest {
    @Test
    public void name() throws Exception {


        PublishSubject<CountdownEvent> publisher = PublishSubject.create();

        Countdown countdown = Countdown.create(publisher);

        TestObserver<CountdownEvent> eventsTest = countdown.observeEvents().test();
        TestObserver<Integer> countdownTest = countdown.observeCounter().test();

        publisher.onNext(new StartCountdownEvent(0, 5));

        countdownTest.await(5500, TimeUnit.MILLISECONDS);

        eventsTest.assertNotComplete().assertValueCount(1);
        countdownTest.assertNotComplete().assertValues(0, 1, 2, 3, 4);

    }

    @Test
    public void name2() throws Exception {
        PublishSubject<CountdownEvent> publisher = PublishSubject.create();

        Countdown countdown = Countdown.create(publisher);

        TestObserver<Timed<CountdownEvent>> eventsTest = countdown.observeEvents()
                .timestamp(TimeUnit.SECONDS)
                .doOnNext(System.out::println)
                .test();

        TestObserver<Timed<Boolean>> test = countdown.isRunning()
                .timestamp(TimeUnit.SECONDS)
                .doOnNext(System.out::println)
                .test();

        countdown.observeCounter()
                .timestamp(TimeUnit.SECONDS)
                .doOnNext(System.out::println)
                .test();

        System.out.println("Publish StartValue");
        publisher.onNext(new StartCountdownEvent(1, 3));

        test.await(3200, TimeUnit.MILLISECONDS);

        eventsTest.assertNotComplete().assertValueCount(3);
        test.assertNotComplete().assertValueCount(3);
    }

    @Test
    public void countdown() throws Exception {
        int from = 5;
        int to = 0;

    }
}
