package stackoverflow.com.rxjavaexamples;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import stackoverflow.com.rxjavaexamples.events.CountdownEvent;
import stackoverflow.com.rxjavaexamples.events.EndCountdownEvent;
import stackoverflow.com.rxjavaexamples.events.StartCountdownEvent;

public class SwitchMapTest {
    @Test
    public void name() throws Exception {
        Subject<CountdownEvent> tObservable = BehaviorSubject.<CountdownEvent>createDefault(new EndCountdownEvent()).toSerialized();

        TestScheduler scheduler = new TestScheduler();

        ConnectableObservable<Integer> replay = tObservable.switchMap(countdownEvent -> {
            if (countdownEvent instanceof StartCountdownEvent) {
                StartCountdownEvent start = (StartCountdownEvent) countdownEvent;

                return Observable.interval(1000, TimeUnit.MILLISECONDS, scheduler).take(start.getCount()).zipWith(Observable.range(start.getStartValue(), start.getCount()), (aLong, integer) -> {
                    return integer;
                }) ;
            }

            return Observable.never();
        }).replay(1);

        replay.connect();

        TestObserver<Integer> test = replay.test();

        tObservable.onNext(new StartCountdownEvent(0, 5));

        scheduler.advanceTimeBy(5500, TimeUnit.MILLISECONDS);

        test.assertNotComplete()
                .assertValues(0, 1, 2, 3, 4);
    }
}

