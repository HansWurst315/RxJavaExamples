package stackoverflow.com.rxjavaexamples;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;

public class DoOnCompleteTest {
    @Test
    public void name() throws Exception {
        Observable<Integer> integerObservable = Observable.interval(0, 1000, TimeUnit.MILLISECONDS)
                .zipWith(Observable.range(0, 5), (aLong, integer) -> {
                    return integer;
                })
                .take(5)
                .doOnTerminate(() -> {
                    System.out.println("finally1");
                })
                .doOnComplete(() -> {
                    System.out.println("complete1");
                }).doOnDispose(() -> {
                    System.out.println("dispose1");
                }).share();

        Observable<Integer> integerObservable1 = Observable.interval(0, 1000, TimeUnit.MILLISECONDS)
                .zipWith(Observable.range(0, 5), (aLong, integer) -> {
                    return integer;
                })
                .take(5)
                .doOnTerminate(() -> {
                    System.out.println("finally2");
                })
                .doOnComplete(() -> {
                    System.out.println("complete2");
                }).doOnDispose(() -> {
                    System.out.println("dispose2");
                });

        Observable<Observable<Integer>> just0 = Observable.just(integerObservable);
        Observable<Observable<Integer>> just1 = Observable.just(integerObservable1)
                .delaySubscription(1000, TimeUnit.MILLISECONDS);

        Observable<Observable<Integer>> merge = Observable.merge(just0, just1);

        Observable<Integer> take = merge
                .doOnNext(integerObservable2 -> System.out.println("SWITCH-_________"))
                .switchMap(obs -> obs);

        take.test().await().assertValueCount(2);
    }

}
