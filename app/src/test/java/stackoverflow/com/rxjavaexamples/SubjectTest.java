package stackoverflow.com.rxjavaexamples;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class SubjectTest {
    @Test
    public void name() throws Exception {
        Subject<Integer> publisher = PublishSubject.<Integer>create();

        Observable<Integer> integerObservable = publisher.switchMap(s -> {
            return Observable.interval(0, 1000, TimeUnit.MILLISECONDS)
                    .zipWith(Observable.range(0, 5), (aLong, integer) -> {
                        return integer;
                    }).take(5).concatWith(Observable.just(6));

            //return Observable.range(0, 5).concatWith(Observable.just(6));
        });

        TestObserver<Integer> test = Observable.merge(publisher, integerObservable)
                .doOnNext(integer -> System.out.println(integer))
                .test();

        publisher.onNext(88);

        Thread.sleep(5500);

        test.assertNotComplete();
        test.assertValueCount(7);
        test.assertValues(88, 0, 1, 2, 3, 4, 6);

        publisher.onNext(88);

        Thread.sleep(5500);

        test.assertNotComplete();
        test.assertValueCount(14);
        test.assertValues(88, 0, 1, 2, 3, 4, 6, 88, 0, 1, 2, 3, 4, 6);
    }

    @Test
    public void sdf() throws Exception {
        Observable.interval(0, 1000, TimeUnit.MILLISECONDS)
                .zipWith(Observable.range(0, 5), (aLong, integer) -> {
                    return integer;
                }).take(5).concatWith(Observable.just(6)).test()
                .await()
                .assertComplete()
                .assertValues(0, 1, 2, 3, 4, 6);
    }
}
